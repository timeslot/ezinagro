package com.timeslot.timeslot;

/**
 * Created by thunderbirdavid on 11/22/15.
 */
public class Time {
    public String time;
    public int votes;
    public String timeId;
    public int color;

    public Time(String time, int votes, String timeId, int color) {
        this.time = time;
        this.votes = votes;
        this.timeId = timeId;
        this.color = color;
    }
}
