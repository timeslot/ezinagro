package com.timeslot.timeslot;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by thunderbirdavid on 11/22/15.
 */
public class EventAdapter extends ArrayAdapter<Event> {
    Context context;
    int resource;
    ArrayList<Event> events = null;

    public EventAdapter(Context context, int resource, ArrayList<Event> events) {
        super(context, resource, events);
        this.context = context;
        this.resource = resource;
        this.events = events;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Event event = events.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(resource, parent, false);
        }

        TextView eventTextView = (TextView)convertView.findViewById(R.id.eventTextView);
        eventTextView.setText(event.name);

        return convertView;
    }
}
