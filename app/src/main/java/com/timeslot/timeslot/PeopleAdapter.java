package com.timeslot.timeslot;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by thunderbirdavid on 11/22/15.
 */
public class PeopleAdapter extends ArrayAdapter<People> {
    Context context;
    int resource;
    ArrayList<People> peoples = null;

    public PeopleAdapter(Context context, int resource, ArrayList<People> peoples) {
        super(context, resource, peoples);
        this.context = context;
        this.resource = resource;
        this.peoples = peoples;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        People people = peoples.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(resource, parent, false);
        }

        TextView peopleTextView = (TextView)convertView.findViewById(R.id.peopleTextView);

        peopleTextView.setText(people.name);
        return convertView;
    }
}