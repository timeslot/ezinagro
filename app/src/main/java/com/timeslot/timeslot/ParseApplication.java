package com.timeslot.timeslot;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseACL;

import com.parse.ParseException;
import com.parse.ParseUser;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class ParseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Add your initialization code here
        Parse.initialize(this, "IIxOE3pvPgH2e3jB9XQC314Lp5EfxtwIYYfhMI2G", "ymBoTxqR3MLsf1PgTviSGG44Do9tZXRJ7dcpB2Sl");

        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();

        // If you would like all objects to be private by default, remove this line.
        defaultACL.setPublicReadAccess(true);

        ParseACL.setDefaultACL(defaultACL, true);
    }

}
