package com.timeslot.timeslot;

/**
 * Created by thunderbirdavid on 11/22/15.
 */
public class Event {
    public String name;
    public String id;

    public Event(String name, String id) { this.name = name; this.id = id;}
}