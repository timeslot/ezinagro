package com.timeslot.timeslot;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

public class EventActivity extends AppCompatActivity {
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    public static ArrayList<People> peoples;
    public static ArrayList<Place> places;
    public static ArrayList<Time> times;
    public static ArrayList<String> peopleStrings;

    public static boolean firstTimeFrag;
    public static boolean firstPlaceFrag;
    public static boolean firstPeopleFrag;

    public static Integer fragInt = 0;

    final Context context = this; // needed for popup to add place, time, and people
    static TimeAdapter timeAdapter = null;
    static PlaceAdapter placeAdapter = null;
    static PeopleAdapter peopleAdapter = null;
    static String eventId = "";
    static String eventName = "";
    public static String userId;

    final String TAG = "EventActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        Intent intent = getIntent();
        eventId = intent.getStringExtra(HomeActivity.EVENT_ID).toString();
        eventName = intent.getStringExtra(HomeActivity.EVENT_NAME).toString();

        ParseUser user = ParseUser.getCurrentUser();
        userId = user.getObjectId();


        times = new ArrayList<Time>();
        places = new ArrayList<Place>();
        peoples = new ArrayList<People>();
        peopleStrings = new ArrayList<String>();

        firstTimeFrag = true;
        firstPlaceFrag = true;
        firstPeopleFrag = true;


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(eventName);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                fragInt = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(), fragStr, Toast.LENGTH_SHORT).show();
                if (fragInt == 0){
                    addPlace();
                } else if (fragInt == 1){
                    addTime2();
                } else if (fragInt == 2){
                    addPerson();
                }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            ParseUser.logOut();
            ParseUser currentUser = ParseUser.getCurrentUser();
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            sharedPreferences.edit().clear().apply();
            finish();
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    //fragStr = "time";
                    return PlaceFragment.newInstance(position + 1);
                case 1:
                    //fragStr = "place";
                    return TimeFragment.newInstance(position + 1);
                case 2:
                    //fragStr = "people";
                    return PeopleFragment.newInstance(position + 1);
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Choices";
                case 1:
                    return "Time";
                case 2:
                    return "People";
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class TimeFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static TimeFragment newInstance(int sectionNumber) {
            TimeFragment fragment = new TimeFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public TimeFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.tab_time, container, false);
            ListView listView = (ListView) rootView.findViewById(R.id.listViewTime);

            timeAdapter = new TimeAdapter(getActivity(), R.layout.row_time, times);
            listView.setAdapter(timeAdapter);

            if (firstTimeFrag) {
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Time");
                query.whereEqualTo("eventId", eventId);
                query.orderByDescending("votes");
                query.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> object, com.parse.ParseException e) {
                        if (e == null) {
                            for (int i = 0; i < object.size(); i++) {
                                if (object.get(i).getList("upVoted").contains(userId)) {
                                    times.add(new Time(object.get(i).getString("time"), object.get(i).getInt("votes"), object.get(i).getObjectId(), 1));
                                }
                                else {
                                    times.add(new Time(object.get(i).getString("time"), object.get(i).getInt("votes"), object.get(i).getObjectId(), 0));
                                }

                                timeAdapter.notifyDataSetChanged();
                            }
                        } else {
                            e.printStackTrace();
                        }
                    }
                });
                firstTimeFrag = false;
            }

            return rootView;
        }
    }

    public static class PlaceFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceFragment newInstance(int sectionNumber) {
            PlaceFragment fragment = new PlaceFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceFragment() {
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.tab_place, container, false);
            ListView listView = (ListView) rootView.findViewById(R.id.listViewPlace);

            placeAdapter = new PlaceAdapter(getActivity(), R.layout.row_place, places);
            listView.setAdapter(placeAdapter);


            if (firstPlaceFrag) {
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Place");
                query.whereEqualTo("eventId", eventId);
                query.orderByDescending("votes");
                query.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> object, com.parse.ParseException e) {
                        if (e == null) {
                            for (int i = 0; i < object.size(); i++) {
                                if (object.get(i).getList("upVoted").contains(userId)){
                                    places.add(new Place(object.get(i).getString("name"), object.get(i).getInt("votes"), object.get(i).getObjectId(), 1));
                                }
                                else if (object.get(i).getList("downVoted").contains(userId)){
                                    places.add(new Place(object.get(i).getString("name"), object.get(i).getInt("votes"), object.get(i).getObjectId(), -1));
                                }
                                else {
                                    places.add(new Place(object.get(i).getString("name"), object.get(i).getInt("votes"), object.get(i).getObjectId(), 0));
                                }


                                placeAdapter.notifyDataSetChanged();
                            }
                        } else {
                            e.printStackTrace();
                        }
                    }
                });
                firstPlaceFrag = false;
            }

            return rootView;
        }
    }

    public static class PeopleFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PeopleFragment newInstance(int sectionNumber) {
            PeopleFragment fragment = new PeopleFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PeopleFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.tab_people, container, false);
            ListView listView = (ListView) rootView.findViewById(R.id.listViewPeople);

            peopleAdapter = new PeopleAdapter(getActivity(), R.layout.row_people, peoples);
            listView.setAdapter(peopleAdapter);

            if (firstPeopleFrag) {
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
                query.getInBackground(eventId, new GetCallback<ParseObject>() {
                    public void done(ParseObject object, ParseException e) {
                        if (e == null) {
                            List<String> usernames = object.getList("usernames");
                            if (usernames != null){
                                for (int i = 0; i < usernames.size(); i++){
                                    peoples.add(new People(usernames.get(i)));
                                    peopleStrings.add(usernames.get(i));
                                    peopleAdapter.notifyDataSetChanged();
                                }
                            }
//                            List<String> idArray = object.getList("userId");
//                            if (idArray != null) {
//                                for (int i = 0; i < idArray.size(); i++) {
//                                    ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
//                                    userQuery.whereEqualTo("objectId", idArray.get(i));
//                                    userQuery.findInBackground(new FindCallback<ParseUser>() {
//                                        public void done(List<ParseUser> objects, ParseException e) {
//                                            if (e == null) {
//                                                peoples.add(new People(objects.get(0).getUsername()));
//                                                peopleAdapter.notifyDataSetChanged();
//                                            } else {
//                                                e.printStackTrace();
//                                            }
//                                        }
//                                    });
//                                }
//                            }
                        } else {
                            e.printStackTrace();
                        }
                    }
                });
                firstPeopleFrag = false;
            }

            return rootView;
        }
    }
    public void addTime2(){
        final View dialogView = View.inflate(this, R.layout.date_time_picker, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        dialogView.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.date_picker);
                TimePicker timePicker = (TimePicker) dialogView.findViewById(R.id.time_picker);

                Calendar calendar = new GregorianCalendar(datePicker.getYear(),
                        datePicker.getMonth(),
                        datePicker.getDayOfMonth(),
                        timePicker.getCurrentHour(),
                        timePicker.getCurrentMinute());

                Date date = calendar.getTime();

                SimpleDateFormat mFormatter = new SimpleDateFormat();
                mFormatter.setTimeZone(TimeZone.getDefault());
                final String time = mFormatter.format(date);

                ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
                query.getInBackground(eventId, new GetCallback<ParseObject>() {
                    public void done(ParseObject object, ParseException e) {
                        if (e == null) {

                            final ParseObject parseTime = new ParseObject("Time");
                            parseTime.put("time", time);
                            parseTime.put("votes", 0);
                            parseTime.put("eventId", eventId);
                            List emptyList = new ArrayList();
                            parseTime.put("upVoted", emptyList);
                            parseTime.put("downVoted", emptyList);

                            ParseACL postACL = new ParseACL(ParseUser.getCurrentUser());
                            postACL.setPublicWriteAccess(true);
                            postACL.setPublicReadAccess(true);
                            parseTime.setACL(postACL);

                            ///ArrayList<ParseObject> parsePlaceArray = (ArrayList<ParseObject>) object.get("places");
                            ///parsePlaceArray.add(parsePlace);
                            //object.put("places", parsePlaceArray);

                            object.saveInBackground();

                            parseTime.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {
                                        times.add(new Time(parseTime.getString("time"), parseTime.getInt("votes"), parseTime.getObjectId(), 0));
                                        timeAdapter.notifyDataSetChanged();

                                    }
                                }
                            });

                        } else {
                            e.printStackTrace();
                        }
                    }
                });
                alertDialog.dismiss();
            }});
        alertDialog.setView(dialogView);
        alertDialog.show();
    }

    public void addTime(){
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View promptView = layoutInflater.inflate(R.layout.prompt_time, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setView(promptView);

        final EditText input = (EditText) promptView.findViewById(R.id.userInput);

        // setup a dialog window
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        // create Parse event object
                        ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
                        query.getInBackground(eventId, new GetCallback<ParseObject>() {
                            public void done(ParseObject object, ParseException e) {
                                if (e == null) {

                                    final ParseObject parseTime = new ParseObject("Time");
                                    parseTime.put("time", input.getText().toString().trim());
                                    parseTime.put("votes", 0);
                                    parseTime.put("eventId", eventId);
                                    List emptyList = new ArrayList();
                                    parseTime.put("upVoted", emptyList);
                                    parseTime.put("downVoted", emptyList);

                                    ParseACL postACL = new ParseACL(ParseUser.getCurrentUser());
                                    postACL.setPublicWriteAccess(true);
                                    postACL.setPublicReadAccess(true);
                                    parseTime.setACL(postACL);

                                    ///ArrayList<ParseObject> parsePlaceArray = (ArrayList<ParseObject>) object.get("places");
                                    ///parsePlaceArray.add(parsePlace);
                                    //object.put("places", parsePlaceArray);

                                    object.saveInBackground();

                                    parseTime.saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            if (e == null) {

                                                times.add(new Time(parseTime.getString("time"), parseTime.getInt("votes"), parseTime.getObjectId(), 0));
                                                timeAdapter.notifyDataSetChanged();

                                            }
                                        }
                                    });

                                } else {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alertD = alertDialogBuilder.create();

        alertD.show();
    }


    public void addPlace(){
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View promptView = layoutInflater.inflate(R.layout.prompt_place, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        // set prompt_event.xmlt.xml to be the layout file of the alertdialog builder
        alertDialogBuilder.setView(promptView);

        final EditText input = (EditText) promptView.findViewById(R.id.userInput);

        // setup a dialog window
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        final String inputString = input.getText().toString().trim();
                        //Check to make sure field isn't empty
                        if (inputString.equals("")) {
                            return;
                        }

                        // create Parse event object
                        ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
                        query.getInBackground(eventId, new GetCallback<ParseObject>() {
                            public void done(ParseObject object, ParseException e) {
                                if (e == null) {

                                    final ParseObject parsePlace = new ParseObject("Place");
                                    parsePlace.put("name", inputString);
                                    parsePlace.put("votes", 0);
                                    parsePlace.put("eventId", eventId);
                                    List emptyList = new ArrayList();
                                    parsePlace.put("upVoted", emptyList);
                                    parsePlace.put("downVoted", emptyList);


                                    ParseACL postACL = new ParseACL(ParseUser.getCurrentUser());
                                    postACL.setPublicWriteAccess(true);
                                    postACL.setPublicReadAccess(true);
                                    parsePlace.setACL(postACL);

                                    ///ArrayList<ParseObject> parsePlaceArray = (ArrayList<ParseObject>) object.get("places");
                                    ///parsePlaceArray.add(parsePlace);
                                    //object.put("places", parsePlaceArray);

                                    object.saveInBackground();

                                    parsePlace.saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            if (e == null) {
                                                places.add(new Place(parsePlace.getString("name"), parsePlace.getInt("votes"), parsePlace.getObjectId(), 0));
                                                placeAdapter.notifyDataSetChanged();
                                            }
                                        }
                                    });
                                } else {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alertD = alertDialogBuilder.create();

        alertD.show();
    }

    public void addPerson(){
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View promptView = layoutInflater.inflate(R.layout.prompt_user, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        // set prompt_event.xmlt.xml to be the layout file of the alertdialog builder
        alertDialogBuilder.setView(promptView);

        final EditText input = (EditText) promptView.findViewById(R.id.userInput);

        // setup a dialog window
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        final String inp = input.getText().toString().trim();
                        // check if Parse username exists
                        ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
                        userQuery.whereEqualTo("username", inp);
                        userQuery.findInBackground(new FindCallback<ParseUser>() {
                            public void done(List<ParseUser> objects, ParseException e) {
                                if (e == null) {
                                    // Parse ensures there are no repeated usernames
                                    if (objects.size() == 0){
                                        Toast.makeText(getApplicationContext(), "User does not exist", Toast.LENGTH_LONG).show();
                                    }
                                    if (objects.size() == 1) {
                                        final ParseUser user = objects.get(0);
                                        // get Parse event object
                                        ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
                                        query.getInBackground(eventId, new GetCallback<ParseObject>() {
                                            public void done(ParseObject object, ParseException e) {
                                                if (e == null) {
                                                    if (peopleStrings.contains(inp)) {
                                                        Toast.makeText(getApplicationContext(), "User already added", Toast.LENGTH_LONG).show();
                                                    } else {
                                                        Log.d(TAG, object.getObjectId());
                                                        object.add("userId", user.getObjectId());
                                                        object.saveInBackground();
                                                        object.add("usernames", inp);
                                                        object.saveInBackground();

                                                        peopleStrings.add(inp);
                                                        peoples.add(new People(inp));
                                                        peopleAdapter.notifyDataSetChanged();
                                                    }
                                                } else {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                                    }
                                } else {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alertD = alertDialogBuilder.create();

        alertD.show();
    }
}
