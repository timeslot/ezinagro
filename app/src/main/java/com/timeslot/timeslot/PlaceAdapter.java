package com.timeslot.timeslot;

import android.content.Context;
import android.graphics.Color;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thunderbirdavid on 11/22/15.
 */
public class PlaceAdapter extends ArrayAdapter<Place> {
    Context context;
    int resource;
    ArrayList<Place> places = null;
    long mLastClickTime = 0;


    public PlaceAdapter(Context context, int resource, ArrayList<Place> places) {
        super(context, resource, places);
        this.context = context;
        this.resource = resource;
        this.places = places;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Place place = places.get(position);
        ParseUser user = ParseUser.getCurrentUser();

        final String placeId = place.placeId;
        final String userId = user.getObjectId();

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(resource, parent, false);
        }

        TextView placeTextView = (TextView)convertView.findViewById(R.id.placeTextView);
        final TextView votesTextView = (TextView)convertView.findViewById(R.id.votesTextView);
        final ImageView upVote = (ImageView)convertView.findViewById(R.id.upVote);
        final ImageView downVote = (ImageView)convertView.findViewById(R.id.downVote);

        placeTextView.setText(place.place);
        votesTextView.setText(place.votes + "");

        if (place.color == 1){
            votesTextView.setTextColor(Color.parseColor("#72CF8A"));
        }
        else if (place.color == -1) {
            votesTextView.setTextColor(Color.parseColor("#FF757F"));
        }

        upVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upVote.setClickable(false);
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Place");
                query.getInBackground(placeId, new GetCallback<ParseObject>() {
                    public void done(ParseObject object, ParseException e) {
                        List<String> upVoted = object.getList("upVoted");
                        List<String> downVoted = object.getList("downVoted");
                        if (e == null) {
                            if (!upVoted.contains(userId)) {
                                if (downVoted.contains(userId)) {
                                    downVoted.remove(userId);
                                    place.votes += 2;
                                } else {
                                    place.votes += 1;
                                }
                                votesTextView.setTextColor(Color.parseColor("#72CF8A"));
                                upVoted.add(userId);
                            }
                            else {
                                upVoted.remove(userId);
                                place.votes -= 1;
                                votesTextView.setTextColor(Color.parseColor("#000000"));
                            }

                            votesTextView.setText(place.votes + "");
                            object.put("upVoted", upVoted);
                            object.put("downVoted", downVoted);
                            object.put("votes", place.votes);
                            object.saveInBackground();
                        } else {
                            e.printStackTrace();
                        }
                        upVote.setClickable(true);
                    }
                });
            }
        });



        downVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 150){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                downVote.setClickable(false);

                ParseQuery<ParseObject> query = ParseQuery.getQuery("Place");
                query.getInBackground(placeId, new GetCallback<ParseObject>() {
                    public void done(ParseObject object, ParseException e) {
                        List<String> upVoted = object.getList("upVoted");
                        List<String> downVoted = object.getList("downVoted");
                        if (e == null) {
                            if (!downVoted.contains(userId)) {
                                if (upVoted.contains(userId)) {
                                    upVoted.remove(userId);
                                    place.votes -= 2;
                                } else {
                                    place.votes -= 1;
                                }
                                votesTextView.setTextColor(Color.parseColor("#FF757F"));
                                downVoted.add(userId);
                            } else {
                                downVoted.remove(userId);
                                place.votes += 1;
                                votesTextView.setTextColor(Color.parseColor("#000000"));
                            }

                            votesTextView.setText(place.votes + "");
                            object.put("upVoted", upVoted);
                            object.put("downVoted", downVoted);
                            object.put("votes", place.votes);
                            object.saveInBackground();
                        } else {
                            e.printStackTrace();
                        }
                        downVote.setClickable(true);

                    };
                });
            }
        });



        return convertView;
    }
}
