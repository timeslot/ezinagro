package com.timeslot.timeslot;

import android.content.Context;
import android.graphics.Color;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thunderbirdavid on 11/22/15.
 */
public class TimeAdapter extends ArrayAdapter<Time>{
    Context context;
    int resource;
    ArrayList<Time> times = null;

    public TimeAdapter(Context context, int resource, ArrayList<Time> times) {
        super(context, resource, times);
        this.context = context;
        this.resource = resource;
        this.times = times;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Time time = times.get(position);
        ParseUser user = ParseUser.getCurrentUser();

        final String timeId = time.timeId;
        final String userId = user.getObjectId();

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(resource, parent, false);
        }

        final TextView timeTextView = (TextView)convertView.findViewById(R.id.timeTextView);
        final TextView votesTextView = (TextView)convertView.findViewById(R.id.votesTextView);
        final ImageView upVote = (ImageView)convertView.findViewById(R.id.upVote);


        timeTextView.setText(time.time);
        votesTextView.setText(time.votes + "");

        if (time.color == 1){
            votesTextView.setTextColor(Color.parseColor("#72CF8A"));
        }

        upVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upVote.setClickable(false);
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Time");
                query.getInBackground(timeId, new GetCallback<ParseObject>() {
                    public void done(ParseObject object, ParseException e) {
                        List<String> upVoted = object.getList("upVoted");
                        int votes = object.getInt("votes");
                        if (e == null && !upVoted.contains(userId)) {
                            object.add("upVoted", userId);


                            votes += 1;

                            object.put("votes", votes);
                            object.saveInBackground();
                            votesTextView.setText(votes + "");
                            votesTextView.setTextColor(Color.parseColor("#72CF8A"));

                        } else if (e == null && upVoted.contains(userId)) {
                            upVoted.remove(userId);
                            object.put("upVoted", upVoted);
                            object.put("votes", votes - 1);
                            object.saveInBackground();
                            votesTextView.setText(votes - 1 + "");
                            votesTextView.setTextColor(Color.parseColor("#000000"));
                        } else {
                            e.printStackTrace();
                        }
                        upVote.setClickable(true);
                    }

                });
            }
        });

        return convertView;
    }
}
