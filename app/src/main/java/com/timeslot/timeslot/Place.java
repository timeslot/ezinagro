package com.timeslot.timeslot;

/**
 * Created by thunderbirdavid on 11/22/15.
 */
public class Place {
    public String place;
    public int votes;
    public String placeId;
    public int color;

    public Place(String place, int votes, String placeId, int color) {
        this.place = place;
        this.votes = votes;
        this.placeId = placeId;
        this.color = color;
    }
}
