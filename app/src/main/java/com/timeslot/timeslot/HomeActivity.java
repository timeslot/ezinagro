package com.timeslot.timeslot;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {
    final Context context = this;
    ArrayList<Event> events = new ArrayList<Event>();
    EventAdapter eventAdapter = null;
    final static String EVENT_ID = "EVENT_ID";
    final static String EVENT_NAME = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ListView listView = (ListView)findViewById(R.id.listViewEvent);
        eventAdapter = new EventAdapter(getApplicationContext(), R.layout.row_event, events);
        listView.setAdapter(eventAdapter);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("My Activities");

        ParseUser user = ParseUser.getCurrentUser();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
        query.whereEqualTo("userId", user.getObjectId());
        query.orderByDescending("updatedAt");
        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> objects, com.parse.ParseException e) {
                if (e == null) {
                    for (int i = 0; i < objects.size(); i++) {
                        events.add(new Event(objects.get(i).getString("name"), objects.get(i).getObjectId()));
                        eventAdapter.notifyDataSetChanged();
                        //Log.d("HomeActivity", "for loop entered");
                    }
                }
            }
        });



        //Challenge!!! Do after doing the previous ones.
        // TODO: 10/30/15 CHALLENGE! Open a new activity that shows that subject's information when a row is clicked.
        //Hint for the challenge: use listView.setOnItemClickListener, as shown below:

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            //Think about the difference between this an a traditional onClickListener
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), EventActivity.class);

                intent.putExtra(EVENT_ID, events.get(position).id);
                intent.putExtra(EVENT_NAME, events.get(position).name);
                startActivity(intent);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // get prompt_event.xmlt.xml view
                LayoutInflater layoutInflater = LayoutInflater.from(context);

                View promptView = layoutInflater.inflate(R.layout.prompt_event, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                // set prompt_event.xmlt.xml to be the layout file of the alertdialog builder
                alertDialogBuilder.setView(promptView);

                final EditText input = (EditText) promptView.findViewById(R.id.userInput);

                // setup a dialog window
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // create Parse event object
                                ParseUser currentUser = ParseUser.getCurrentUser();
                                final ParseObject event = new ParseObject("Event");

                                //ArrayList<ParseObject> places = new ArrayList<ParseObject>();
                                //event.put("places", places);

                                String inputString = input.getText().toString().trim();
                                if (inputString.equals("")) {
                                    return;
                                }

                                event.put("name", inputString);
                                event.add("userId", currentUser.getObjectId());
                                event.add("usernames", currentUser.getUsername());

                                ParseACL postACL = new ParseACL(ParseUser.getCurrentUser());
                                postACL.setPublicWriteAccess(true);
                                postACL.setPublicReadAccess(true);
                                event.setACL(postACL);

                                event.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if (e == null){
                                            events.add(0, new Event(event.getString("name"), event.getObjectId()));
                                            eventAdapter.notifyDataSetChanged();

                                        }
                                    }
                                });
                            }
                        })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                // create an alert dialog
                AlertDialog alertD = alertDialogBuilder.create();

                alertD.show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_event, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            ParseUser.logOut();
            ParseUser currentUser = ParseUser.getCurrentUser();
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            sharedPreferences.edit().clear().apply();
            finish();
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
